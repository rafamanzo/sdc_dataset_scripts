import csv
import sys
import os
import h5py
import numpy as np

filename = os.path.basename(sys.argv[1]).split('.')[0]
file = h5py.File(sys.argv[1], 'r+')

fieldnames = list(file.keys())
extended_fieldnames = []
for field in fieldnames:
    if isinstance(file[field][0], np.ndarray):
        for dim in range(len(file[field][0])):
            extended_fieldnames.append(field+'_dim_'+str(dim))
    else:
        extended_fieldnames.append(field)

rows = file['cam1_ptr'].shape[0]

with open(filename+'.csv', 'w') as csvfile:
  writer = csv.DictWriter(csvfile, fieldnames=extended_fieldnames)
  writer.writeheader()

  for row in range(rows):
    row_data = {}
    for field in fieldnames:
        if row < len(file[field]):
            if isinstance(file[field][row], np.ndarray):
                for dim in range(len(file[field][row])):
                    dim_field = field+'_dim_'+str(dim)
                    row_data[dim_field] = file[field][row][dim]
            else:
                row_data[field] = file[field][row]
    writer.writerow(row_data)
