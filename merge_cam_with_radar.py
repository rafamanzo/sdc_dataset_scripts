import sys
import os
import multiprocessing
import PIL

from PIL import Image


def resize_and_merge(index):
    filename = str(index).zfill(frame_digits_count)+'.png'

    try:
        images = [Image.open(img) for img in [cam_frames_path+'/'+cam_frames[index], radar_imgs_path+'/'+radar_imgs[index]]]
        _, radar_height = images[1].size
        cam_width, cam_height = images[0].size
        aspect = cam_width/cam_height

        images[0] = images[0].resize((int(radar_height*aspect),radar_height), PIL.Image.ANTIALIAS)

        # See: http://stackoverflow.com/questions/30227466/combine-several-images-horizontally-with-python
        widths, heights = zip(*(i.size for i in images))

        total_width = sum(widths)
        max_height = max(heights)

        # Ensure divisibility by 2, so ffmpeg works
        if total_width % 2 == 1:
            total_width += 1
        if max_height % 2 == 1:
            max_height += 1

        new_im = Image.new('RGB', (total_width, max_height))

        x_offset = 0
        for im in images:
          new_im.paste(im, (x_offset,0))
          x_offset += im.size[0]

        new_im.save(filename)

        for img in images:
            img.close()
        new_im.close()
    except IOError as excep:
        print(filename)
        print(excep)


cam_frames_path = sys.argv[1]
radar_imgs_path = sys.argv[2]


frame_digits_count = len(str(len(os.listdir(radar_imgs_path))))
merged_frame_index = 0
merged_frame_indices = []
cam_frames = []
radar_imgs = []

for radar_img in sorted(os.listdir(radar_imgs_path)):
    if radar_img.endswith('.png'):
        cam_frame = radar_img.split('.')[0].split('_')[0]+'.png'

        if not os.path.isfile(radar_imgs_path+'/'+radar_img):
            print('Not found '+radar_img)
            continue

        if not os.path.isfile(cam_frames_path+'/'+cam_frame):
            print('Not found '+cam_frame)
            continue

        merged_frame_indices.append(merged_frame_index)
        cam_frames.append(cam_frame)
        radar_imgs.append(radar_img)

        merged_frame_index +=1

process_count = multiprocessing.cpu_count()
pool = multiprocessing.Pool(process_count)

pool.map(resize_and_merge, merged_frame_indices)
