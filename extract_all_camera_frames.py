import numpy as np
import h5py
import sys
from scipy.misc import imsave
import multiprocessing


def generate_frames_for_slice(part):
    frame = None
    for frame_num in range(part[0], part[1]):
        imsave(str(frame_num).zfill(digits_count) + '.png', np.rollaxis(file['X'][frame_num], 0, 3))


file_mutex = multiprocessing.Lock()
file = h5py.File(sys.argv[1], 'r')

frames = file['X'].shape[0]
digits_count = len(str(frames))
# h5py is not Thread safe for reading
# If you really need it, check: http://docs.h5py.org/en/latest/mpi.html
process_count = 1# multiprocessing.cpu_count()

slices = []
slice_size = int(frames/process_count)
start = 0

pool = multiprocessing.Pool(process_count)

for _ in range(process_count - 1):
    slices.append((start, start + slice_size))
    start += slice_size

slices.append((start, frames))

pool.map(generate_frames_for_slice, slices)
