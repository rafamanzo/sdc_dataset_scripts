import csv
import os
import sys


csvfilename = sys.argv[1]
sorted_csvfilename = os.path.splitext(csvfilename)[0]+'_sorted.csv'


data = []
with open(csvfilename) as f:
    reader = csv.reader(f)
    data = list(reader)

headers = data[0]
data = data[1:]

cam1_ptr_index = headers.index('cam1_ptr')
data.sort(key=lambda row: int(float(row[cam1_ptr_index])))

with open(sorted_csvfilename, 'w') as f:
    writer = csv.writer(f)
    writer.writerow(headers)
    writer.writerows(data)
