import multiprocessing
import csv
import sys
import os


csv_file_path = sys.argv[1]


filename_prefix = os.path.splitext(os.path.basename(csv_file_path))[0]
split_count = multiprocessing.cpu_count()
splat_csv_files = []

headers = []
row_count = 0
with open(csv_file_path) as csvfile:
    reader = csv.DictReader(csvfile)
    headers = list(next(reader).keys())
    row_count = 1
    for _ in reader:
        row_count += 1
headers.append('row_num')

split_index = 0
for _ in range(split_count):
    splat_csv_files.append(open(filename_prefix+str(split_index)+'.csv', 'w'))
    split_index += 1

splat_csv_writers = [csv.DictWriter(csvfile, fieldnames=headers) for csvfile in splat_csv_files]
for writer in splat_csv_writers:
    writer.writeheader()

slice_size = int(row_count/split_count)

row_num = 0
writer_index = 0
with open(csv_file_path) as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        row['row_num'] = row_num
        splat_csv_writers[writer_index].writerow(row)

        row_num +=1

        if row_num >= (writer_index*slice_size + slice_size) and writer_index < (len(splat_csv_writers) -1):
            writer_index += 1

for csvfile in splat_csv_files:
    csvfile.close()