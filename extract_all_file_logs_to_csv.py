import os
import sys
import multiprocessing
import pipes

def extract_log_to_csv_for(dataset_name):
    input_data = pipes.quote(data_path+'/'+dataset_name+'.h5')
    dest_path = results_path+'/log/'+dataset_name
    if not os.path.exists(dest_path):
        os.makedirs(dest_path)

    os.chdir(dest_path)

    os.system('python '+scripts_path+'/log_to_csv.py '+input_data)


scripts_path = os.path.dirname(os.path.realpath(__file__))
data_path = sys.argv[1]
results_path = sys.argv[2]


names = [os.path.splitext(file)[0] for file in os.listdir(data_path)]

process_count = multiprocessing.cpu_count()

pool = multiprocessing.Pool(process_count)
pool.map(extract_log_to_csv_for, names)
