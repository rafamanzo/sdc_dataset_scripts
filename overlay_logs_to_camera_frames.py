import numpy as np
import h5py
import sys
import multiprocessing
import matplotlib.pyplot as plt
import math


def generate_frames_for_slice(part):
    data_labels = list(normalization.keys())
    fontsize = 5

    fig, ax = plt.subplots(1, 2, gridspec_kw={'width_ratios': [1, 0.25]})

    ax[0].get_xaxis().set_visible(False)
    ax[0].get_yaxis().set_visible(False)
    ax[1].set_ylim([0,len(data_labels)])
    ax[1].set_xlim([0,1])
    ax[1].spines['top'].set_visible(False)
    ax[1].spines['right'].set_visible(False)
    ax[1].set_yticks(np.arange(1, len(data_labels), 1))
    ax[1].set_yticklabels(
        data_labels,
        rotation='horizontal',
        fontsize=fontsize,
    )
    for label in ax[1].get_xticklabels():
        label.set_fontsize(fontsize)

    for row_num in range(part[0], part[1]):
        frame_num = log_file['cam1_ptr'][row_num]

        ax[0].imshow(np.rollaxis(cam_file['X'][frame_num], 0, 3))

        bar_y = 1.0
        for label, data in normalization.items():
            if data['dim'] is None:
                bar_x = (log_file[data['orig_field']][row_num] - normalization[field]['min'])*normalization[field]['const']
            else:
                bar_x = (log_file[data['orig_field']][row_num][data['dim']] - normalization[field]['min'])*normalization[field]['const']
            ax[1].barh(bar_y, bar_x, label=label, color=data['color'], height=0.5)
            bar_y += 1

        plt.tight_layout()
        plt.savefig(str(row_num).zfill(digits_count) + '.png')


log_file = h5py.File(sys.argv[1], 'r+')
cam_file = h5py.File(sys.argv[2], 'r+')

fieldnames = list(log_file.keys())
fieldnames.remove('UN_D_cam1_ptr')
fieldnames.remove('UN_D_lidar_ptr')
fieldnames.remove('UN_D_radar_msg')
fieldnames.remove('UN_D_rawgps')
fieldnames.remove('UN_T_cam1_ptr')
fieldnames.remove('UN_T_lidar_ptr')
fieldnames.remove('UN_T_radar_msg')
fieldnames.remove('UN_T_rawgps')
fieldnames.remove('cam1_ptr')
fieldnames.remove('times')

extended_fieldnames = []
normalization = {}
for field in fieldnames:
    if isinstance(log_file[field][0], np.ndarray):
        for dim in range(len(log_file[field][0])):
            if (isinstance(log_file[field][0][dim], int) or isinstance(log_file[field][0][dim], float)):
                    extended_fieldnames.append(field+'_dim_'+str(dim))

                    normalization[extended_fieldnames[-1]] = {}
                    normalization[extended_fieldnames[-1]]['min'] = log_file[field][:][dim].min()
                    normalization[extended_fieldnames[-1]]['const'] = 1.0/(log_file[field][:][dim].max() - normalization[extended_fieldnames[-1]]['min'])
                    normalization[extended_fieldnames[-1]]['orig_field'] = field
                    normalization[extended_fieldnames[-1]]['dim'] = dim
                    normalization[extended_fieldnames[-1]]['color'] = np.random.rand(3,1)

    elif (isinstance(log_file[field][0], int) or isinstance(log_file[field][0], float)):
        extended_fieldnames.append(field)

        normalization[extended_fieldnames[-1]] = {}
        normalization[extended_fieldnames[-1]]['min'] = log_file[field][:].min()
        normalization[extended_fieldnames[-1]]['const'] = 1.0/(log_file[field][:].max() - normalization[extended_fieldnames[-1]]['min'])
        normalization[extended_fieldnames[-1]]['orig_field'] = field
        normalization[extended_fieldnames[-1]]['dim'] = None
        normalization[extended_fieldnames[-1]]['color'] = np.random.rand(3,1)

for field in extended_fieldnames:
    if math.isnan(normalization[field]['min']) or math.isnan(normalization[field]['const']) or math.isinf(normalization[field]['min']) or math.isinf(normalization[field]['const']):
        normalization.pop(field, None)

rows = log_file[list(normalization.keys())[0]].shape[0]
row_num = 0
digits_count = len(str(rows))
process_count = multiprocessing.cpu_count()

slices = []
slice_size = int(rows/process_count)
start = 0

pool = multiprocessing.Pool(process_count)

#for _ in range(process_count - 1):
#    slices.append((start, start + slice_size))
#    start += slice_size
#slices.append((start, rows))
slices.append((start, 1))

pool.map(generate_frames_for_slice, slices)
