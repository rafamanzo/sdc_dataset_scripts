import os
import sys


dataset = sys.argv[1]
scripts_path = os.path.dirname(os.path.realpath(__file__))
base_path = os.getcwd()
cam_dir = base_path+'/'+dataset+'_cam/'
radar_dir = base_path+'/'+dataset+'_radar/'

os.mkdir(cam_dir)
os.mkdir(radar_dir)
os.system('s3cmd sync s3://commadataset-results/camera/'+dataset+'/ '+cam_dir)
os.system('s3cmd sync s3://commadataset-results/radar/'+dataset+'/ '+radar_dir)
os.mkdir(base_path+'/merged')
os.mkdir(base_path+'/merged/'+dataset)
os.chdir(base_path+'/merged/'+dataset)
os.system('python '+scripts_path+'/merge_cam_with_radar.py '+cam_dir+' '+radar_dir)
os.system("ffmpeg -r 100 -f image2 -pattern_type glob -i '*.png' -vcodec libx264 -crf 25  -pix_fmt yuv420p video.mp4")
os.system('s3cmd sync --rr '+base_path+'/merged/ s3://commadataset-results/merged/')
