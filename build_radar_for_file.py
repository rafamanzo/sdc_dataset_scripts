import os
import sys


dataset = sys.argv[1]
scripts_path = os.path.dirname(os.path.realpath(__file__))
base_path = os.getcwd()


os.system('s3cmd get s3://commadataset-results/log/'+dataset+'/'+dataset+'.csv')
os.system('s3cmd get s3://commadataset/log/'+dataset+'.h5')
os.mkdir(base_path+'/csvs')
os.chdir(base_path+'/csvs')
os.system('python '+scripts_path+'/split_csv.py '+base_path+'/'+dataset+'.csv')
os.chdir(base_path)
os.mkdir(base_path+'/radar')
os.chdir(base_path+'/radar')
os.mkdir(base_path+'/radar/'+dataset)
os.chdir(base_path+'/radar/'+dataset)
os.system('python '+scripts_path+'/csv_log_to_radar.py '+base_path+'/'+dataset+'.h5 '+base_path+'/csvs')
os.system('s3cmd sync --rr '+base_path+'/radar/ s3://commadataset-results/radar/')
