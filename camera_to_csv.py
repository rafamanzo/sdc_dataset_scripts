import csv
import sys
import os
import h5py

filename = os.path.basename(sys.argv[1]).split('.')[0]
file = h5py.File(sys.argv[1], 'r+')

fieldnames = ['frame', 'r', 'g', 'b', 'x', 'y']

rows = file['X'].shape[0]

with open(filename+'.csv', 'w') as csvfile:
  writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
  writer.writeheader()

  for frame_num in range(file['X'].shape[0]):
    row_data = {'frame': frame_num}
    for x in range(file['X'].shape[2]):
        row_data['x'] = x
        for y in range(file['X'].shape[3]):
            row_data['y'] = y

            row_data['r'] = file['X'][frame_num][0][x][y]
            row_data['g'] = file['X'][frame_num][1][x][y]
            row_data['b'] = file['X'][frame_num][2][x][y]

            writer.writerow(row_data)
