import numpy as np
import h5py
import sys
import multiprocessing
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import math


def generate_frames_for_slice(part):
    data_labels = list(normalization.keys())
    fontsize = 6
    log_bg_color = '#aBa9aE'

    grid_spec = gridspec.GridSpec(1, 2, width_ratios=[1, 0.75])
    frame_axis = plt.subplot(grid_spec[0])
    log_axis = plt.subplot(grid_spec[1], projection='polar')

    frame_axis.get_xaxis().set_visible(False)
    frame_axis.get_yaxis().set_visible(False)

    theta_step = 2*np.pi/len(data_labels)
    log_axis.get_yaxis().set_visible(False)
    #log_axis.get_xaxis().set_visible(False)
    log_axis.grid(False)
    log_axis.set_ylim([0,1.0])
    log_axis.set_xticks(np.arange(0, 2*np.pi, theta_step))
    log_axis.set_xticklabels(
        [normalization[data_label]['abrev'] for data_label in data_labels],
        fontsize=fontsize,
        family='monospace',
    )
    log_axis.set_facecolor(log_bg_color)

    rotation = 0
    rotation_step = 360/len(data_labels)
    for label in log_axis.get_xticklabels():
        if rotation > 90 and rotation < 270:
          label.set_rotation(180+rotation)
        else:
          label.set_rotation(rotation)
        label.set_fontsize(fontsize)
        rotation += rotation_step

    plt.tight_layout()
    for row_num in range(part[0], part[1]):
        frame_num = log_file['cam1_ptr'][row_num]

        frame_axis.imshow(np.rollaxis(cam_file['X'][frame_num], 0, 3))

        theta = 0.0
        radius = 0.0
        for label, data in normalization.items():
            if data['dim'] is None:
                radius = (log_file[data['orig_field']][row_num] - normalization[field]['min'])*normalization[field]['const']
            else:
                radius = (log_file[data['orig_field']][row_num][data['dim']] - normalization[field]['min'])*normalization[field]['const']

            log_axis.bar(theta, 1.0, label=label, width=theta_step, color=log_bg_color, edgecolor='black', linestyle='-', zorder=0)
            log_axis.bar(theta, radius, label=label, width=theta_step, color=data['color'], zorder=1)
            theta += theta_step

        #plt.legend(fontsize=fontsize, ncol=2, bbox_to_anchor=[0.95, 0.5])
        plt.savefig(str(row_num).zfill(digits_count) + '.png', bbox_inches='tight')


log_file = h5py.File(sys.argv[1], 'r+')
cam_file = h5py.File(sys.argv[2], 'r+')

fieldnames = list(log_file.keys())
fieldnames.remove('UN_D_cam1_ptr')
fieldnames.remove('UN_D_lidar_ptr')
fieldnames.remove('UN_D_radar_msg')
fieldnames.remove('UN_D_rawgps')
fieldnames.remove('UN_T_cam1_ptr')
fieldnames.remove('UN_T_lidar_ptr')
fieldnames.remove('UN_T_radar_msg')
fieldnames.remove('UN_T_rawgps')
fieldnames.remove('cam1_ptr')
fieldnames.remove('times')

extended_fieldnames = []
normalization = {}
for field in fieldnames:
    if isinstance(log_file[field][0], np.ndarray):
        for dim in range(len(log_file[field][0])):
            if (isinstance(log_file[field][0][dim], int) or isinstance(log_file[field][0][dim], float)):
                    extended_fieldnames.append(field+'_dim_'+str(dim))

                    normalization[extended_fieldnames[-1]] = {}
                    normalization[extended_fieldnames[-1]]['min'] = log_file[field][:][dim].min()
                    normalization[extended_fieldnames[-1]]['const'] = 1.0/(log_file[field][:][dim].max() - normalization[extended_fieldnames[-1]]['min'])
                    normalization[extended_fieldnames[-1]]['orig_field'] = field
                    normalization[extended_fieldnames[-1]]['dim'] = dim
                    normalization[extended_fieldnames[-1]]['color'] = np.random.rand(3,1)
                    normalization[extended_fieldnames[-1]]['abrev'] = ''
                    for word in extended_fieldnames[-1].split('_'):
                        normalization[extended_fieldnames[-1]]['abrev'] += word[0]
    elif (isinstance(log_file[field][0], int) or isinstance(log_file[field][0], float)):
        extended_fieldnames.append(field)

        normalization[extended_fieldnames[-1]] = {}
        normalization[extended_fieldnames[-1]]['min'] = log_file[field][:].min()
        normalization[extended_fieldnames[-1]]['const'] = 1.0/(log_file[field][:].max() - normalization[extended_fieldnames[-1]]['min'])
        normalization[extended_fieldnames[-1]]['orig_field'] = field
        normalization[extended_fieldnames[-1]]['dim'] = None
        normalization[extended_fieldnames[-1]]['color'] = np.random.rand(3,1)
        normalization[extended_fieldnames[-1]]['abrev'] = ''
        for word in extended_fieldnames[-1].split('_'):
            normalization[extended_fieldnames[-1]]['abrev'] += word[0]

for field in extended_fieldnames:
    if math.isnan(normalization[field]['min']) or math.isnan(normalization[field]['const']) or math.isinf(normalization[field]['min']) or math.isinf(normalization[field]['const']):
        normalization.pop(field, None)

# https://i0.wp.com/imgs.xkcd.com/blag/assorted_colors.png
colors = [
    '#7e1e9c',
    '#aBa9aE',
    '#0343df',
    '#ff81c0',
    '#653700',
    '#e50000',
    '#95d0fc',
    '#029386',
    '#f97306',
    '#96f97b',
    '#c20078',
    '#ffff14',
    '#75bbfd',
    '#15b01a',
    '#89fe05',
    '#bf77f6',
    '#9a0eea',
    '#033500',
    '#06c2ac',
    '#c79f2f',
    '#00035b',
    '#d1b26f',
    '#00ffff',
    '#13eac9',
    '#06470c',
    '#ae7181',
    '#35063e',
    '#01ff07',
    '#650021',
    '#6e750e',
    '#ff796c',
    '#e6daa6',
    '#0504aa',
    '#001146',
    '#cea2fd',
    '#000000',
    '#ff028d',
    '#ad8150',
    '#c7fdb5',
    '#ffb07c',
    '#677a04',
    '#cb416b',
    '#8e82fe',
    '#53fea1',
    '#aaff32',
    '#380282',
    '#ceb301',
    '#ffd1df',
]

color = 0
for field, data in normalization.items():
    data['color'] = colors[color]
    color += 1

rows = log_file['cam1_ptr'].shape[0]
row_num = 0
digits_count = len(str(rows))
process_count = multiprocessing.cpu_count()

slices = []
slice_size = int(rows/process_count)
start = 0

pool = multiprocessing.Pool(process_count)

for _ in range(process_count - 1):
    slices.append((start, start + slice_size))
    start += slice_size
slices.append((start, rows))

pool.map(generate_frames_for_slice, slices)
