import numpy as np
import h5py
import sys
import matplotlib.pyplot as plt

file = h5py.File(sys.argv[1], 'r+')
frame = file['X'][int(sys.argv[2])]

plt.imshow(np.rollaxis(frame, 0, 3))
plt.show()